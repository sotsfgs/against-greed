extends AudioStreamPlayer

onready var theme1 = load("res://audio/Dark City.ogg")
onready var theme2 = load("res://audio/level 1thtme.ogg")

func play_this(song, db=0):
	self.stop()
	self.volume_db = db
	self.stream = song
	self.play()
	
func is_playing_this(song):
	if self.stream == song:
		return true
		
	return false