extends MarginContainer

onready var play_btn = get_node("hdiv/menu/options/Play")
onready var exit_btn = get_node("hdiv/menu/options/Exit")

func _ready():
	play_btn.connect("pressed", self, "on_btn_play")
	exit_btn.connect("pressed", self, "on_btn_exit")
	if !music.is_playing_this(music.theme1):
		music.play_this(music.theme1)

func on_btn_play():
	# Rock 'n' Roll!
	switcher.goto_scene("res://scenes/tutotial.tscn")
	
func on_btn_exit():
	# Bye!
	music.stop()
	get_tree().quit()