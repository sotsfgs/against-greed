extends HBoxContainer

onready var money = get_node("money")
onready var victories = get_node("victories")
onready var points = get_node("points")

func _settext():
	money.text = "MONEY: %d" % playerinfo.money
	victories.text = "VICTORIES: %d" % playerinfo.victories
	points.text = "POINTS: %.3f" % playerinfo.points

func _ready():
	_settext()
	
func _process(delta):
	_settext()