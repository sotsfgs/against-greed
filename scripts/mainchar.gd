extends KinematicBody2D

export var speed = 300
onready var sprite = get_node("Sprite")

var animstate = "idle_south"
var turnflipoff = ["idle_south", "idle_north", "walk_south", "walk_north"]
export var stand_still = false

func _ready():
	playerinfo.hero = get_node(".")

func _play_again():
	stand_still = false
	switcher.goto_scene("res://scenes/tutotial.tscn")

func _go_to_menu():
	playerinfo.reset_stats()
	switcher.goto_scene("res://scenes/Main Menu World.tscn")

func _process(delta):
	if stand_still:
		return

	var motion = Vector2()
	
	if Input.is_action_pressed("char_left"):
		motion += Vector2(-1, 0)
		animstate = "walk_side"
		sprite.flip_h = true
	elif Input.is_action_just_released("char_left"):
		animstate = "idle_side"
		sprite.flip_h = true
		
	if Input.is_action_pressed("char_up"):
		motion += Vector2(0, -1)
		animstate = "walk_north"
	elif Input.is_action_just_released("char_up"):
		animstate = "idle_north"
		
	if Input.is_action_pressed("char_right"):
		motion += Vector2(1, 0)
		animstate = "walk_side"
		sprite.flip_h = false
	elif Input.is_action_just_released("char_right"):
		animstate = "idle_side"
		sprite.flip_h = false
		
	if Input.is_action_pressed("char_down"):
		motion += Vector2(0, 1)
		animstate = "walk_south"
	elif Input.is_action_just_released("char_down"):
		animstate = "idle_south"
		
	motion = motion.normalized()*speed*delta
	if sprite.animation != animstate:
		sprite.animation = animstate
	
	if animstate in turnflipoff:
		sprite.flip_h = false
	
	move_and_collide(motion)
	
func add_money(value):
	playerinfo.money += value
	playerinfo.totalmoney += value
	if playerinfo.minigame or stand_still:
		return
	if playerinfo.money % 5 == 0:
		playerinfo.minigame = true
		stand_still = true
		switcher.ad.start()