extends Node2D

onready var hero_position = Vector2()
onready var tween = get_node("Tween")
onready var player = get_node("AnimationPlayer")
onready var touch = get_node("collision")
export var value = 1
export var follow = true

func _ready():
	touch.connect("body_entered", self, "on_body_enter")

func _process(delta):
	if !follow:
		return

	var hero = playerinfo.hero
	
	if hero != null:
		if hero_position != hero.global_position:
			hero_position = hero.global_position
			var distance = self.global_position.distance_to(hero_position)
			if distance <= 300:
				tween_bill(hero_position, hero)
		
				
func tween_bill(end_pos, object):
	tween.interpolate_property(self, "global_position", self.global_position, end_pos, 0.2, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	tween.start()
	
func on_body_enter(body):
	if body.get_name() == "mainchar":
		set_process(false)
		tween.stop(self, "global_position")
		player.play("poof")
		body.add_money(self.value)