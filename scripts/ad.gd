extends Panel

var copy1 = "RESIST THE TEMPTATION\nDON'T BUY THINGS YOU DON'T NEED!"
var copy2 = "FIGHT IT OR YOU LOSE IN..."
var bar = 0
var will = 0
var power = 1
export var countdown = 3
var total = 0
var coefficient = 0

onready var coins = get_tree().get_nodes_in_group("coins").size()
onready var temptation = get_node("tempt")
onready var lose = get_node("lose")
onready var copy = get_node("MarginContainer/VBoxContainer/copy")
onready var timelbl = get_node("MarginContainer/VBoxContainer/row/time")
onready var progress = get_node("MarginContainer/VBoxContainer/row/progress")

func _ready():
	switcher.ad = get_node(".")
	show_advice()
	connect("hide", self, "on_hide")
	temptation.connect("timeout", self, "on_timeout")
	lose.connect("timeout", self, "on_timeout_lose")
	calculate_difficulty()

func reset_stats():
	countdown = 3
	total = coins / 2
	temptation.stop()
	lose.stop()
	hide()
	
func on_hide():
	playerinfo.hero.stand_still = false
	playerinfo.minigame = false
	
func show_advice():
	copy.text = copy1
	progress.show()
	timelbl.hide()

func show_timer():
	copy.text = copy2
	progress.hide()
	timelbl.show()
	timelbl.text = "%d sec." % countdown

func _process(delta):
	if countdown == 0:
		reset_stats()
		var points = will * coefficient
		playerinfo.moneylose += 1
		playerinfo.victories -= 1
		playerinfo.points -= points
		
		if playerinfo.victories <= 0:
			# Game over!
			switcher.popup.popup_centered()

	if total <= 0:
		# el jugador ha logrado resistir la tentacion
		reset_stats()
		# ejecutar animacion de victoria?
		var points = will * coefficient
		playerinfo.victories += 1
		playerinfo.totalvictories += 1
		playerinfo.totalpoints += points
		playerinfo.points += points

	if total >= bar and lose.is_stopped():
		# el jugador no esta resistiendo la tentacion
		show_timer()
		total = bar
		temptation.stop()
		lose.start()

	if Input.is_action_just_released("resist") and self.visible:
		# El jugador trata de resistir
		show_advice()
		lose.stop()
		if temptation.is_stopped():
			temptation.start()
		countdown = 3
		total -= will

	progress.set_as_ratio(total / bar)
		
func start():
	calculate_difficulty()
	# TODO: recalculate the temptation timer time
	show_advice()
	show()
	temptation.start()
	
func calculate_difficulty():
	# The longitude of the bar should be equal to the number of coins in the map
	bar = float(coins)
	coefficient = power * pow(1.07, playerinfo.totalmoney)
	will = 1
	# Start with the coefficient
	total = bar / 4
	
	temptation.wait_time += .07
	
func on_timeout():
	total += coefficient
	
func on_timeout_lose():
	if countdown > 0:
		countdown -= 1
		timelbl.text = "%d sec." % countdown