extends Node2D

onready var area = get_node("Area2D")

func _ready():
	area.connect("body_entered", self, "on_body_enter")
	
func on_body_enter(body):
	if body.get_name() == playerinfo.hero.get_name():
		body.stand_still = true
		body.sprite.animation = "idle_south"
		switcher.popup.popup()