extends ConfirmationDialog

func _ready():
	switcher.popup = get_node(".")
	connect("confirmed", playerinfo.hero, "_play_again")
	connect("about_to_show", self, "on_about_to_be_shown")
	get_cancel().connect("pressed", playerinfo.hero, "_go_to_menu")
	
func on_about_to_be_shown():
	dialog_text = "Total points: %.3f\nTotal Money: %d\nTotal Victories: %d\nWanna play again?" % [playerinfo.totalpoints, playerinfo.totalmoney, playerinfo.totalvictories]