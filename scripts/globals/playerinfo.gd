extends Node

var money = 0
var moneylose = 1
var totalmoney = 0
var victories = 0
var totalvictories = 0
var totalpoints = 0.0
var points = 0.0
var hero = null
var minigame = false

func reset_stats():
	money = 0
	moneylose = 1
	victories = 0
	totalpoints = 0.0
	points = 0.0
	totalvictories = 0
	totalmoney = 0