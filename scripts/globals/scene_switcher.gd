extends MarginContainer

var rloader
var wait_frames
var current_scene = null
var popup = null
var ad = null

func _ready():
	var root = get_tree().get_root()
	current_scene = root.get_child( root.get_child_count() -1 )
	
func goto_scene(path):
	# Load new scene
	rloader = ResourceLoader.load_interactive(path)
	if rloader == null: # check for errors
		print("Can't load %s" % path)
		return
	set_process(true)
	
	current_scene.queue_free()
	wait_frames = 1

func set_new_scene(res):
	current_scene = res.instance()
	get_tree().get_root().add_child(current_scene)
	
func update_progress():
	var progress = float(rloader.get_stage()) / rloader.get_stage_count()
	get_node("vbox/hbox/bar").set_as_ratio(progress)

func _process(delta):
	if rloader == null:
		set_process(false)
		return
		
	if wait_frames > 0:
		wait_frames -= 1
		return
		
	var tick = OS.get_ticks_msec()
	
	while OS.get_ticks_msec() < tick + 100:
		var err = rloader.poll()
		
		if err == ERR_FILE_EOF:
			var resource = rloader.get_resource()
			rloader = null
			set_new_scene(resource)
			break
		elif err == OK:
			update_progress()
		else:
			rloader = null
			break
